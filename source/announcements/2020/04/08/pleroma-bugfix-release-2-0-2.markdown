title: Pleroma patch release: 2.0.2
date: 2020-04-08 10:00 UTC
tags: ["Release", "stable"]
author: rinpatch

Pleroma 2.0.2 is a patch release, fixing bugs found after 2.0.1 release.

<!--more-->
## Backend changes

### Added
- Support for Funkwhale's `Audio` activity
- Admin API: `PATCH /api/pleroma/admin/users/:nickname/update_credentials`

### Fixed
- Blocked/muted users still generating push notifications
- Input textbox for bio ignoring newlines
- OTP: Inability to use PostgreSQL databases with SSL
- `user delete_activities` breaking when trying to delete already deleted posts
- Incorrect URL for Funkwhale channels

## Pleroma-FE changes

### Fixed
- Favorite/Repeat avatars not showing up on private instances/non-public posts
- Autocorrect getting triggered in the captcha field
- Overflow on long domains in follow/move notifications

## Admin-FE changes
### Added
- Ability to see local statuses in Statuses by instance section
- Ability to configure Oban.Cron settings and settings for notifications streamer
- Ability to set user's password and email on user's page
- Display status count by scope on Statuses page

### Changed
- Link to Pleroma docs when a non-admin user tries to log in 

## Upgrade notes
1. Restart Pleroma

