title: Pleroma security release: 2.0.1
date: 2020-03-15 21:23 UTC
tags: ["stable", "Release", "Security"]
author: rinpatch

Pleroma 2.0.1 is a security release, fixing improper HTML sanitization in Static-FE and bugs found after 2.0.0 release.

<!--more-->
## Backend changes

### Security
- Static-FE: Fix remote posts not being sanitized

### Removed
- **Breaking:** `fetch_initial_posts` option

### Fixed
- Rate limiter crashes when there is no explicitly specified ip in the config
- 500 errors when no `Accept` header is present if Static-FE is enabled
- Instance panel not being updated immediately due to wrong `Cache-Control` headers
- Statuses posted with BBCode/Markdown having unncessary newlines in Pleroma-FE
- OTP: Fix some settings not being migrated to in-database config properly
- No `Cache-Control` headers on attachment/media proxy requests
- Character limit enforcement being off by 1
- Mastodon Streaming API: hashtag timelines not working

### Changed
- BBCode and Markdown formatters will no longer return any `\n` and only use `<br/>` for newlines
- Mastodon API: Allow registration without email if email verification is not enabled


## Admin-FE changes

### Added

- Ability to see local statuses in Statuses by instance section
- Ability to configure Oban.Cron settings and settings for notifications streamer

### Fixed

- Fix parsing tuples in Pleroma.Upload.Filter.Mogrify and Pleroma.Emails.Mailer settings

## Upgrade notes
### Nginx only
1. Remove `proxy_ignore_headers Cache-Control;` and `proxy_hide_header  Cache-Control;` from your config.

### Everyone
1. Run database migrations (inside Pleroma directory):
  - OTP: `./bin/pleroma_ctl migrate`
  - From Source: `mix ecto.migrate`
2. Restart Pleroma

