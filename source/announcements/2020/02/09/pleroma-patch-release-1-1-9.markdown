title: Pleroma patch release: 1.1.9
date: 2020-02-09 12:08 UTC
tags: ["stable", "Release"]
author: rinpatch

Pleroma 1.1.9 is a patch release, fixing bugs found after 1.1.8 release.

<!--more-->

## Fixed

- OTP: Inability to set the upload limit (again)
- Not being able to pin polls
- Streaming API: incorrect handling of reblog mutes
- Rejecting the user when field length limit is exceeded
- OpenGraph provider: HTML entities in descriptions
