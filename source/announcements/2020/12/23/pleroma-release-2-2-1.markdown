title: Pleroma patch release: 2.2.1
date: 2020-12-23 11:45 UTC
tags: ["stable", "Release"]
author: lain, shp

Pleroma 2.2.1 is patch release that fixes a few backend bugs.

On the frontend side we've got a long list of improvements in this release.

Most noticeable changes are moving the "external source" under the ellipsis button. Speaking of buttons, many of them have much more generous hitboxes for better mobile use and they should be much more accessible for keyboard navigation plugins.

One cool new feature is enabling use of flat colors for the background. For users you can use it by removing your own personal background and making sure in general settings that you're not using instance default background. For instance admins that want to use the flat color by default, you can just remove the background image on your instance.

The emoji reactions have major improvements as you can now input emoji directly into the field, the ordering of the emoji is better and it includes some emoji that were previously missing.
<!--more-->

## Upgrade notes

1. Restart Pleroma

## Frontend changes

### Added
- Mouseover titles for emojis in reaction picker
- Support to input emoji into the search box in reaction picker
- Added some missing unicode emoji
- Added the upload limit to the Features panel in the About page
- Support for solid color wallpaper, instance doesn't have to define a wallpaper anymore

### Fixed
- Fixed the occasional bug where screen would scroll 1px when typing into a reply form
- Fixed timeline errors locking timelines
- Fixed missing highlighted border in expanded conversations
- Fixed custom emoji not working in profile field names
- Fixed pinned statuses not appearing in user profiles
- Fixed some elements not being keyboard navigation friendly
- Fixed error handling when updating various profile images
- Fixed your latest chat messages disappearing when closing chat view and opening it again during the same session
- Fixed custom emoji not showing in poll options before voting
- Fixed link color not applied to instance name in topbar
- Fixed regression in react popup alignment and overflowing

### Changed
- Errors when fetching are now shown with popup errors instead of "Error fetching updates" in panel headers
- Made reply/fav/repeat etc buttons easier to hit
- Adjusted timeline menu clickable area to match the visible button
- Moved external source link from status heading to the ellipsis menu
- Disabled horizontal textarea resize
- Wallpaper is now top-aligned, horizontally centered.

## Backend changes

### Fixed

- Config generation: rename `Pleroma.Upload.Filter.ExifTool` to `Pleroma.Upload.Filter.Exiftool`.
- S3 Uploads with Elixir 1.11.
- Mix task pleroma.user delete_activities for source installations.
- Search: RUM index search speed has been fixed.
- Rich Media Previews sometimes showed the wrong preview due to a bug following redirects.
- Fixes for the autolinker.
- Forwarded reports duplication from Pleroma instances.

- <details>
    <summary>API</summary>
  - Statuses were not displayed for Mastodon forwarded reports.
  </details>

