title: Pleroma security release: 2.0.4
date: 2020-05-10 16:55 UTC
tags: ["Release", "stable", "Security"]
author: rinpatch

Pleroma 2.0.4 is a security release, fixing a potentially breaky migration introduced in 2.0.3,
a potential DoS using AP C2S and other bugs found since 2.0.3 release.

<!--more-->

## Backend changes
 
### Security
- AP C2S: Fix a potential DoS by creating nonsensical objects that break timelines

### Fixed
- Peertube user lookups not working
- `InsertSkeletonsForDeletedUsers` migration failing on some instances
- Healthcheck reporting the number of memory currently used, rather than allocated in total
- LDAP not being usable in OTP releases
- Default apache configuration having tls chain issues

## Upgrade notes

#### Apache only

1. Remove the following line from your config:

```
    SSLCertificateFile      /etc/letsencrypt/live/${servername}/cert.pem
```

#### Everyone

1. Restart Pleroma
