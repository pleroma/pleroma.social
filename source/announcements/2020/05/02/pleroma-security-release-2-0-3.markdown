title: Pleroma security release: 2.0.3
date: 2020-05-02 12:36 UTC
tags: ["stable", "Release", "Security"]
author: rinpatch

Pleroma 2.0.3 is a security release, fixing: possibility of re-registration of previously deleted users, ability to force a follow from a local user, and bugs found after 2.0.2 release.

<!--more-->

## Backend changes

### Security
- Disallow re-registration of previously deleted users, which allowed viewing direct messages addressed to them
- Mastodon API: Fix `POST /api/v1/follow_requests/:id/authorize` allowing to force a follow from a local user even if they didn't request to follow
- CSP: Sandbox uploads

### Fixed
- Notifications from blocked domains
- Potential federation issues with Mastodon versions before 3.0.0
- HTTP Basic Authentication permissions issue
- Follow/Block imports not being able to find the user if the nickname started with an `@`
- Instance stats counting internal users
- Inability to run a From Source release without git
- ObjectAgePolicy didn't filter out old messages
- `blob:` urls not being allowed by CSP

### Added
- Follow request notifications
- NodeInfo: ObjectAgePolicy settings to the `federation` list.
- <details>
  <summary>API Changes</summary>
  - Admin API: `GET /api/pleroma/admin/need_reboot`.
</details>

## Pleroma-FE changes

### Fixed
- Show more/less works correctly with auto-collapsed subjects and long posts
- RTL characters won't look messed up in notifications

### Changed
- Emoji autocomplete will match any part of the word and not just start, for example :drool will now helpfully suggest :blobcatdrool: and :blobcatdroolreach:

### Added
- Follow request notification support

## Admin-FE changes

### Added

- Link settings that enable registrations and invites

### Changed

- Put Instance Reboot button on all pages of admin-fe
- Make Instance Reboot button's positon fixed on Settings page
- Update jest and babel-jest
- Generate an invite link when an invite token has been generated
- Put labels on top of inputs in mobile version
- Shorten suggestions for a series of select inputs: Rewrite policy, Pleroma Authenticator, Captcha Method, Mailer adapter, Metadata providers, Rich Media parsers, TTL setters, Scrub policy, Uploader, Filters and Federation publisher modules

### Fixed

- Disable Invites tab when invites are disabled on BE

## Upgrade notes

1. Consider stopping pleroma before the migrations to avoid security issues, it should just take few minutes.
2. Run database migrations (inside Pleroma directory):
  - OTP: `./bin/pleroma_ctl migrate`
  - From Source: `mix ecto.migrate`
3. (Re)start Pleroma
