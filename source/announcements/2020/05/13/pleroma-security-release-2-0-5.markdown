title: Pleroma security release: 2.0.5
date: 2020-05-13 10:00 UTC
tags: ["stable", "Release", "Security"]
author: rinpatch

Pleroma 2.0.5 is a security release, fixing a potential private status leak in Streaming API,
removes the hard dependency on `erlang-eldap` introduced in 2.0.4 and other bugs found since 2.0.4 release.

<!--more-->

## Backend changes

### Security
- Fix possible private status leaks in Mastodon Streaming API

### Fixed
- Crashes when trying to block a user if block federation is disabled
- Not being able to start the instance without `erlang-eldap` installed
- Users with bios over the limit getting rejected
- Follower counters not being updated on incoming follow accepts

## Pleroma-FE changes

### Added
- Private notifications option for push notifications
- 'Copy link' button for statuses (in the ellipsis menu)

### Changed
- Registration page no longer requires email if the server is configured not to require it

### Fixed
- Status ellipsis menu closes properly when selecting certain options

## Upgrade notes

1. Restart Pleroma
