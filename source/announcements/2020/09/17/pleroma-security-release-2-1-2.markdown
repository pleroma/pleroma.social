title: Pleroma security release: 2.1.2
date: 2020-09-17 18:02 UTC
tags: ["Release", "stable"]
author: rinpatch

Pleroma 2.1.2 is a security release, fixing some object types (most notably polls) bypassing MRF and fixing bugs found after 2.1.1 release.

<!--more-->

## Upgrade notes

1. Restart Pleroma

## Backend changes

### Security

- Fix most MRF rules either crashing or not being applied to objects passed into the Common Pipeline (ChatMessage, Question, Answer, Audio, Event).

### Fixed

- Welcome Chat messages preventing user registration with MRF Simple Policy applied to the local instance.
- Mastodon API: the public timeline returning an error when the `reply_visibility` parameter is set to `self` for an unauthenticated user.
- Mastodon Streaming API: Handler crashes on authentication failures, resulting in error logs.
- Mastodon Streaming API: Error logs on client pings.
- Rich media: Log spam on failures. Now the error is only logged once per attempt.

### Changed

- Rich Media: A HEAD request is now done to the url, to ensure it has the appropriate content type and size before proceeding with a GET.

## Frontend changes

### Fixed
- Fixed chats list not updating its order when new messages come in
- Fixed chat messages sometimes getting lost when you receive a message at the same time

