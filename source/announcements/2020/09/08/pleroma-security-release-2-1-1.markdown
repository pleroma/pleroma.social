title: Pleroma security release: 2.1.1
date: 2020-09-08 12:42 UTC
tags: ["Release", "stable"]
author: rinpatch

Pleroma 2.1.1 is a security release, fixing 2 DoS vulnerabilities, metadata leak on private instances,
a possible OOM with the default HTTP client and bugs found after 2.1.0 release.

<!--more-->
## Backend changes

### Security
- Fix possible DoS in Mastodon API user search due to an error in match clauses, leading to an infinite recursion and subsequent OOM with certain inputs.
- Fix metadata leak for accounts and statuses on private instances.
- Fix possible DoS in Admin API search using an atom leak vulnerability. Authentication with admin rights was required to exploit.

### Changed

- **Breaking:** The metadata providers RelMe and Feed are no longer configurable. RelMe should always be activated and Feed only provides a <link> header tag for the actual RSS/Atom feed when the instance is public.
- Improved error message when cmake is not available at build stage.

### Added
- Rich media failure tracking (along with `:failure_backoff` option).

### Fixed
- Default HTTP adapter not respecting pool setting, leading to possible OOM.
- Fixed uploading webp images when the Exiftool Upload Filter is enabled by skipping them
- Mastodon API: Search parameter `following` now correctly returns the followings rather than the followers
- Mastodon API: Timelines hanging for (`number of posts with links * rich media timeout`) in the worst case.
Reduced to just rich media timeout.
- Mastodon API: Cards being wrong for preview statuses due to cache key collision.
- Password resets no longer processed for deactivated accounts.
- Favicon scraper raising exceptions on URLs longer than 255 characters.

## Pleroma-FE changes

### Changed
- Polls will be hidden with status content if "Collapse posts with subjects" is enabled and the post is collapsed.

### Fixed
- Autocomplete won't stop at the second @, so it'll still work with "@lain@l" and not start over.
- Fixed weird autocomplete behavior when you write ":custom_emoji: ?"

## Upgrade notes
1. Restart Pleroma
