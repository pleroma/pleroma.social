title: Pleroma security release: 2.0.7
date: 2020-06-13 08:46 UTC
tags: ["stable", "Release", "Security"]
author: rinpatch

Pleroma 2.0.7 is a security release, fixing 2 potential DoSes and CSP regressions introduced in 2.0.6 release.

<!--more-->

## Backend Changes

### Security
- Fix potential DoSes exploiting atom leaks in rich media parser and the `UserAllowListPolicy` MRF policy

### Fixed
- CSP: not allowing images/media from every host when mediaproxy is disabled
- CSP: not adding mediaproxy base url to image/media hosts
- StaticFE missing the CSS file

## Upgrade notes

1. Restart Pleroma

