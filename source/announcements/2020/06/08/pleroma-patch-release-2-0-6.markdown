title: Pleroma patch release: 2.0.6
date: 2020-06-08 18:55 UTC
tags: ["stable", "Release"]
author: rinpatch

Pleroma 2.0.6 is a patch release, bringing some database performance improvements, security hardening and
 fixing bugs found after the 2.0.5 release.

<!--more-->

## Backend changes

### Security
- CSP: harden `image-src` and `media-src` when MediaProxy is used

### Fixed
- AP C2S: Fix pagination in inbox/outbox
- Various compilation errors on OTP 23
- Mastodon API streaming: Repeats from muted threads not being filtered

### Changed
- Various database performance improvements

## Upgrade notes
1. Run database migrations (inside Pleroma directory):
  - OTP: `./bin/pleroma_ctl migrate`
  - From Source: `mix ecto.migrate`
2. Restart Pleroma


