title: Rate limiter and Remote IP plug soon to be enabled by default again
date: 2020-01-25 10:28 UTC
tags: ["develop", "stable"]
author: rinpatch

[Rate limiter](https://docs-develop.pleroma.social/backend/configuration/cheatsheet/#rate_limit) was disabled by default in [!1601](https://git.pleroma.social/pleroma/pleroma/merge_requests/1601) since the majority of Pleroma instances were behind reverse proxies and it didn't take `X-Forwarded-For` headers into account, rate limiting the reverse proxy IP instead.

<!-- more -->

Since then a [Remote IP plug](https://docs-develop.pleroma.social/backend/configuration/cheatsheet/#pleromaplugsremoteip) was added to get the real IP from headers, but wasn't enabled by default because the default nginx config didn't forward the header and allowed spoofing it.

If you use any other reverse proxy we officially support other than nginx, no action is required on your part. Otherwise add the following to your `location /` block:

```
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
```

If you want to explicitly disable rate limiting and the Remote IP plug (i.e if you run a tor/i2p instance) you can do that by adding the following to your config:


```
config :pleroma, Pleroma.Plugs.RemoteIp, enabled: false

config :pleroma, :rate_limit, nil
```

**EDIT 2020-01-26T22:34:57Z:** Removed a statement about not needing to do anything if the instance was set up after September 2019. The nginx config fix has not made it into the stable branch yet.
