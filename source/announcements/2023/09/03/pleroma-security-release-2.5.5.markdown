title: Pleroma security release: 2.5.5
date: 2023-09-03 00:00 UTC
tags: ["stable", "Release", "security"]
author: lanodan

Pleroma 2.5.5 is a security release. Prevents users from accessing media of other users by creating a status with reused attachment ID

<!-- more -->

## Upgrade notes

### From source only
1. Pull updates
2. Recompile Pleroma:
```
MIX_ENV=prod mix compile
```
3. Restart Pleroma

### Everyone

1. Update and Restart Pleroma

## Frontend changes

None.

## Backend changes

### Security
- Prevent users from accessing media of other users by creating a status with reused attachment ID
