title: Separating Frontend and Backend versions
date: 2023-11-09 00:00 UTC
tags: ["release-cycle", "changes", "for-admins"]
author: hj

> **Notice:**   It was previously stated that version split starts with 2.6.0 but due to a serious bug in frontend this has been postponed to 2.6.1 release

Hi, it's HJ again. For past few years we had same recocurring problem - frontend has brand new features, but to release it out to public we need to also release backend so newer frontend version would be bundled in it, and backend might not be ready for release at the moment, or backend maintainers might be away/busy, or there is simply nothing to release there (I don't think latter ever happened though). That essentially means that while development is still happening, most people won't see it until next backend release which might be months away. However there is a problem, how do you use a non-built frontend?

## Using a non-builtin frontend 

While it was pretty much always possible to use a custom version of PleromaFE, it was always a hassle to set it up. The very first way of doing this was essentially building PleromaFE yourself, which is an extremely involved process for uninitiated, even when just doing a `master` or `develop` build, and maintaining your own branch would be even more involved; in short - not for overwhelming majority of people. Later we added support for multiple frontends and a way to install them in AdminFE, but it is very clunky and confusing, and while it could be used to _install_ a PleromaFE build from our GitLab, it was hardcoded to only one branch - `master`, and it would __not__ let you _update_ it without manually specifying build URL and such. Selecting which frontend is actually being served by default is also clunky, you have to manually enter frontend name and version, even though this information is readily available.

Given that AdminFE is on "life support" at the moment as the original developer of it vanished and how much work it is to get it into appropriate state we started working on porting AdminFE features into PleromaFE, now referred to as "Admin Dashboard". Existing AdminFE still available and still supported, but don't expect massive changes there, and eventually it will be replaced, we just don't know when. Apart from some (better organized) extremely basic instance settings in Admin Dashboard one key feature of it that shipped in 2.6.0 release is Front-ends management.

### Front-ends management

Accessing Admin Dashboard is in the same place where old AdminFE is accessed, in the "dashboard" icon in top bar. Don't worry, there is a link to old AdminFE within it, and direct link should still work.

![Screenshot of part of PleromaFE pointing out location of Admin Dashboard location](/images/admin_dash_location.png)

From there, just open "Front-ends" tab, where you can easily (re)install any frontends and set them as default in an (hopefully) easier way, in addition we added option to install `develop` version of PleromaFE.

![Screenshot of part of PleromaFE Admin Dashboard pointing out the Front-ends tab location](/images/frontends_tab.png)

Further information is in our [documentation](https://docs-develop.pleroma.social/backend/administration/frontends-management/)

*There is a known bug in 2.6.0 that there is __no feedback__ whether frontend is installing, installed or failed to install, and the dropdowns don't close on click - all of those are fixed in PleromaFE 2.6.1.*

*There is a known bug in 2.6.0 that if you have not primary frontend set already it won't load the interface for setting one, this is fixed in 2.6.1*

## Splitting the versions

Version 2.6.1 is the last one to be fully synchronized between frontend and backend. **From now on, backend and frontend versions no longer will be in sync**

Announcements of new releases will be made for both Backend releases and Frontend releases.

### TL;DR:
PleromaFE will be more of a "Pleroma client" that aims to support most versions of Pleroma backend with graceful degradation where appropriate. We feasibly cannot support all older and newer versions ever so boundaries of support will shift periodically.

### Current plan in detail

* Backend can implement new features, but it doesn't necessarily mean those features are instantly implemented in bundled frontend.
* When backend does their release current stable (i.e. `master` branch) version will be included as a built-in.
* Frontend will support:
  * The "bottom line" is Backend version 2.6.0, versions older than that may still work but are not officially supported.
    * We still accept patches that fixes support for older versions but discretion will be used whether to accept them or not, i.e. extremely complicated fix or fix that breaks support for newer versions most likely won't be accepted as-is.
  * The "top line" is yet to be determined and will be announced if support for some versions is being dropped.
  * For most features support should still work across all backend versions - future and past
    * Graceful degradation where appropriate, if there's a new way to post or read timelines in a better way in newer backend version it won't be supported in older version of frontend and vice-versa.
    * The "bottom line" might be moved to newer version if some core API is deprecated/removed in favor of new one
    * Features are detected not by checking backend version but rather by what backend reports it supports.
  * Features that are in active development between Backend and Frontend (i.e. at time of writing - Admin Dashboard) might not guaranteed to work outside specific version combinations until said features are stabilized.

  As usual, nothing is set in stone and might change over time.

  T. HJ
