title: Pleroma security release: 2.5.2
date: 2023-05-26 00:00 UTC
tags: ["stable", "Release", "security"]
author: lanodan

Pleroma 2.5.2 is a security release. Featuring many fixes, additions and improvements.

<!-- more -->

## Upgrade notes

### From source only
1. Get new dependencies and recompile Pleroma:

    ```
    MIX_ENV=prod mix deps.get
    MIX_ENV=prod mix compile
    ```

### Everyone

1. Run database migrations (inside Pleroma directory):
  * OTP: ``./bin/pleroma_ctl migrate``
  * From Source: ``mix ecto.migrate``
2. Recommended: Run `VACUUM ANALYZE` on your database
3. Restart Pleroma

## Frontend changes

None.

## Backend changes

### Security
- `/proxy` endpoint now sets a Content-Security-Policy (sandbox)
- WebSocket endpoint now respects unauthenticated restrictions for streams of public posts
- OEmbed HTML tags are now filtered

### Changed
- docs: Be more explicit about the level of compatibility of OTP releases
- Set default background worker timeout to 15 minutes

### Fixed
- Atom/RSS formatting (HTML truncation, published, missing summary)
- Remove `static_fe` pipeline for `/users/:nickname/feed`
- Stop oban from retrying if validating errors occur when processing incoming data
- Make sure object refetching as used by already received polls follows MRF rules

### Removed
- BREAKING: Support for passwords generated with `crypt(3)` (Gnu Social migration artifact)

## Admin Frontend changes

None.
