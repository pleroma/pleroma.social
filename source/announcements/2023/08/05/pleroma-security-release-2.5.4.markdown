title: Pleroma security release: 2.5.4
date: 2023-08-05 00:00 UTC
tags: ["stable", "Release", "security"]
author: lanodan

Pleroma 2.5.4 is a security release. Fixes a file loading vulnerability via XML External Entity (XXE).

<!-- more -->

## Upgrade notes

### From source only
1. Recompile Pleroma:

    ```
    MIX_ENV=prod mix compile
    ```

### Everyone

1. Restart Pleroma

## Frontend changes

None.

## Backend changes

### Security
- Fix XML External Entity (XXE) loading vulnerability allowing to fetch arbitrary files from the server's filesystem (reported by [@Mae@is.badat.dev](https://is.badat.dev/users/Mae))
