title: Pleroma security release: 2.5.3
date: 2023-08-04 00:00 UTC
tags: ["stable", "Release", "security"]
author: lanodan

Pleroma 2.5.3 is a security release. Fixes one path-traversal vulnerability, and hardens permissions.

<!-- more -->

## Upgrade notes

### From source only
1. Recompile Pleroma:

    ```
    MIX_ENV=prod mix compile
    ```

### Everyone

1. Restart Pleroma

## Frontend changes

None.

## Backend changes

### Security
- Emoji pack loader sanitizes pack names
- Reduced permissions of config files and directories, distros requiring greater permissions like group-read need to pre-create the directories
