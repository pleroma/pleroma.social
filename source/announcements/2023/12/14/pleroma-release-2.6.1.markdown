title: Pleroma minor release: 2.6.1
date: 2023-12-14 00:00 UTC
tags: ["stable", "Release"]
author: tusooa

Pleroma 2.6.1 is a minor release. It features some bugfixes, notably fixing a bug that prevents you from managing frontends if you have not set a primary frontend. After this release, the frontend and backend versions will no longer be in sync. See [this relevant post](/announcements/2023/11/9/release-cycle-news/) for more information.

<!-- more -->

## Upgrade notes

### From source only
1. Pull updates
2. Recompile Pleroma:
```
MIX_ENV=prod mix compile
```
3. Restart Pleroma

### Everyone

1. Update and Restart Pleroma

## Frontend changes
### Fixed
- fix admin dashboard not having any feedback on frontend installation
- Fix frontend admin tab crashing when no primary frontend is set
- Add aria attributes to react and extra buttons

## Backend changes
### Changed
- - Document maximum supported version of Erlang & Elixir

### Added
- [docs] add frontends management documentation

### Fixed
- TwitterAPI: Return proper error when healthcheck is disabled
- Fix eblurhash and elixir-captcha not using system cflags
