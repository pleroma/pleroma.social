title: Pleroma security release: 2.6.2
date: 2024-02-20 00:00 UTC
tags: ["stable", "Release", "security"]
author: lanodan

Pleroma 2.6.2 is a security release. Fixes lack of shortcode sanitization in MRF StealEmojiPolicy.

<!-- more -->

## Upgrade notes

### From source only
1. Pull updates
2. Recompile Pleroma:
```
MIX_ENV=prod mix compile
```
3. Restart Pleroma

### Everyone

1. Update and Restart Pleroma

## Frontend changes

None.

## Backend changes

### Security
- MRF StealEmojiPolicy: Sanitize shortcodes (thanks to Hazel K for the report)
