title: Pleroma security release: 2.6.3
date: 2024-05-22 00:00 UTC
tags: ["stable", "Release", "security"]
author: lain

Pleroma 2.6.3 is a security release. It fixes an issue with webfinger queries not properly checking the account domain, leading to impersonation.

<!-- more -->

## Upgrade notes

### From source only
1. Pull updates
2. Recompile Pleroma:
```
MIX_ENV=prod mix compile
```
3. Restart Pleroma

### Everyone

1. Update and Restart Pleroma

## Frontend changes

None.

## Backend changes

### Security
- Fix webfinger spoofing.
