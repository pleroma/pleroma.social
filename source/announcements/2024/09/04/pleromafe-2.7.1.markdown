title: PleromaFE Minor Release: 2.7.1
date: 2024-09-04 00:00 UTC
tags: ["stable", "Release", "PleromaFE"]
author: hj

PleromaFE 2.7.1 is a minor release of the frontend that fixes some outstanding issues found in 2.7.0 release.

You can update frontend separately from backend, see [documentation](https://docs-develop.pleroma.social/backend/administration/frontends-management/).

<!-- more -->

## Notes
Bugfix release. Added small optimizations to emoji picker that should make it a bit more responsive, however it needs rather large change to make it more performant which might come in a major release.

### Fixed
- Instance default theme not respected
- Nested panel header having wrong sticky position if navbar height != panel header height
- Toggled buttons having bad contrast (when using v2 theme)

### Changed
- Simplify the OAuth client_name to 'PleromaFE'
- Small optimizations to emoji picker
