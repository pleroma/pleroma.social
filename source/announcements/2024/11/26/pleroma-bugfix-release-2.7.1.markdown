title: Pleroma Bugfix Release: 2.7.1
date: 2024-11-26 00:00 UTC
tags: ["stable", "Release"]
author: lanodan

Pleroma 2.7.1 is a bugfix release. Please read through the changelog to see the changes in detail.

<!-- more -->

## Upgrade notes

### From source only
1. Get new dependencies and recompile Pleroma:

    ```
    MIX_ENV=prod mix deps.get
    MIX_ENV=prod mix compile
    ```
2. Restart Pleroma

### Everyone

1. Restart Pleroma

## Backend changes

### Changed
- Accept `application/activity+json` for requests to `/.well-known/nodeinfo`

### Fixed
- Truncate remote user fields, avoids them getting rejected
- Improve the `FollowValidator` to successfully incoming activities with an errant `cc` field.
- Resolved edge case where the API can report you are following a user but the relationship is not fully established.
- The Swoosh email adapter for Mailgun was missing a new dependency on `:multipart`
- Fix Mastodon WebSocket authentication
