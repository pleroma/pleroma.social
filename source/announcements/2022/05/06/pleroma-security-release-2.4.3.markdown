title: Pleroma security release: 2.4.3
date: 2022-05-06 10:00 UTC
tags: ["stable", "Release"]
author: lanodan

Pleroma 2.4.3 is a security release. Notably fixing a cache issue which can leak private Activities and Objects.

<!-- more -->

## Upgrade notes

### From source only
1. Get new dependencies and recompile Pleroma:

    ```
    MIX_ENV=prod mix deps.get
    MIX_ENV=prod mix compile
    ```

### Everyone

1. Restart Pleroma

## Frontend changes

None

## Backend changes

### Security
- Private `/objects/` and `/activities/` leaking if cached by authenticated user
- SweetXML library DTD bomb
