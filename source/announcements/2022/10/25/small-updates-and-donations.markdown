title: Small updates and donation pages
date: 2022-10-20 20:20 EEST
tags: ["community"]
author: hj

Hi, we're busy working on 2.5 release and reorganizing. In meantime we've done 
some small things.

## Development progress insights

There are plans on setting up unified feed ("Planet") but for now HJ's been doing
series of updates on his blog about happenings in PleromaFE's development:
https://ebin.club/hj-blog/tags/pleroma-fe/

## Merchandise

Semi-official, non-profit, derivative merchandise available here:
https://ebin-shop.myspreadshop.fi/lainsoft+pleroma?collection=KvbpYBOlKJ made by HJ

## Donation pages

You can now donate to the cause. Never mandatory but always appreciated.
- Libera pay group: https://liberapay.com/Pleroma-euro
  - Donations go directly to people listed, of course you can donate to individuals
    separately if you want.
- Open Collective: https://opencollective.com/pleroma-euro
  - Donations will go towards infrastructure, paid tools and possibly other things
