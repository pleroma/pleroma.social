title: Pleroma patch release: 2.4.2
date: 2022-01-11 14:00 UTC
tags: ["stable", "Release"]
author: rinpatch

Pleroma 2.4.2 is a patch release. It fixes potential federation issues, makes Pleroma compatible with Elixir 1.13 and features reworked mention display in Pleroma-FE, among other changes.

<!-- more -->

## Upgrade notes

### From source only
1. Get new dependencies and recompile Pleroma:

    ```
    MIX_ENV=prod mix deps.get
    MIX_ENV=prod mix compile
    ```

### Everyone

1. Restart Pleroma

## Frontend changes

### Changed
- Show mentions on separate line if they are not in the post content
- Collapse mentions that come after the first 5

### Added
- Added Apply and Reset buttons to the bottom of theme tab to minimize UI travel
- Implemented user option to always show floating New Post button (normally mobile-only)
- Display reasons for instance specific policies
- Added functionality to cancel follow request

### Fixed
- Fixed link to external profile not working on user profiles
- Fixed mobile shoutbox display
- Fixed favicon badge not working in Chrome
- Escape html more properly in subject/display name

## Backend changes

### Fixed
- Federation issues caused by HTTP pool checkout timeouts
- Compatibility with Elixir 1.13
