title: Erlang 24.2-1 is broken on Arch Linux
date: 2022-01-10 14:58 UTC
tags: ["develop", "stable"]
author: rinpatch

Version 24.2-1 (latest at time of writing) of `erlang` and `erlang-nox` packages on Arch Linux [ships](https://bugs.archlinux.org/task/73240?project=5&string=erlang) empty libraries, resulting in inability to compile native code and therefore run Pleroma from source.

<!-- more -->

If you see errors like this while trying to compile Pleroma - you are affected:

```
/usr/bin/ld: /tmp/ccrLNKWF.o: in function `handle_send':
/opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:181: undefined reference to `ei_x_new'
/usr/bin/ld: /opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:185: undefined reference to `ei_decode_version'
/usr/bin/ld: /opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:190: undefined reference to `ei_decode_tuple_header'
/usr/bin/ld: /opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:197: undefined reference to `ei_decode_atom'
/usr/bin/ld: /opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:222: undefined reference to `ei_get_type'
/usr/bin/ld: /opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:234: undefined reference to `ei_decode_binary'
/usr/bin/ld: /opt/pleroma/deps/fast_html/c_src/fasthtml_worker.c:238: undefined reference to `ei_decode_list_header'
[...]
```

To fix the issue, downgrade the package to version `24.1.7-1` or earlier.

With `erlang-nox` package the following command, ran as root, should solve the issue:

```
pacman -U https://archive.archlinux.org/packages/e/erlang-nox/erlang-nox-24.1.7-1-x86_64.pkg.tar.zst
```
