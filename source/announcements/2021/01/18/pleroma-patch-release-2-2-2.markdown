title: Pleroma patch release: 2.2.2
date: 2021-01-18 14:11 UTC
tags: ["stable", "Release"]
author: rinpatch

Pleroma 2.2.2 is a patch release. It fixes minor annoyances on the backend side, like EmojiStealPolicy not creating a directory by itself and `mix deps.get` warning about a retired package.

On the frontend side it adds a report button to the status menu, fixes issues with displaying Follows/Followers and more.

<!-- more -->

## Upgrade notes

1. Restart Pleroma

## Frontend changes

### Added
- Added Report button to status ellipsis menu for easier reporting

### Fixed
- Follows/Followers tabs on user profiles now display the content properly.
- Handle punycode in screen names

### Changed
- Don't filter own posts when they hit your wordfilter

## Backend changes

### Fixed

- StealEmojiPolicy creates dir for emojis, if it doesn't exist.
- Updated `elixir_make` to a non-retired version
