title: Move from Freenode to Libera.Chat
date: 2021-05-31T15:14:00 UTC
tags: ["community"]
author: lanodan

The Pleroma project is moving it's chatrooms together with the former freenode staff to [Libera.Chat](https://libera.chat/) and now considers the chatrooms on Freenode to be unofficial.

This also means a move of the matrix chatrooms to [#pleroma:libera.chat](https://matrix.to/#/#pleroma:libera.chat) and [#pleroma-dev:libera.chat](https://matrix.to/#/#pleroma-dev:libera.chat).

Our TheLounge instance at <https://irc.pleroma.social> has already been updated to point to Libera.Chat.

See you on Libera.Chat!
