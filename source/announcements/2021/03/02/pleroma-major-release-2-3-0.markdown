title: Pleroma major release: 2.3.0
date: 2021-03-02 20:09 UTC
tags: ["stable", "Release"]
author: lain

Pleroma 2.3.0 has been released, featuring many fixes, additions and improvements.

<!-- more -->

## Upgrade notes

1. Run database migrations (inside Pleroma directory):
  - OTP: `./bin/pleroma_ctl migrate`
  - From Source: `mix ecto.migrate`
2. Restart Pleroma

## Frontend changes

### Fixed
- Button to remove uploaded media in post status form is now properly placed and sized.
- Fixed shoutbox not working in mobile layout
- Fixed missing highlighted border in expanded conversations again
- Fixed some UI jumpiness when opening images particularly in chat view
- Fixed chat unread badge looking weird
- Fixed punycode names not working properly

### Changed
- Display 'people voted' instead of 'votes' for multi-choice polls
- Optimized chat to not get horrible performance after keeping the same chat open for a long time
- When opening emoji picker or react picker, it automatically focuses the search field
- Language picker now uses native language names

### Added
- Added reason field for registration when approval is required
- Group staff members by role in the About page

## Backend changes

### Security

- Fixed client user agent leaking through MediaProxy

### Removed

- `:auth, :enforce_oauth_admin_scope_usage` configuration option.

### Changed

- **Breaking**: Changed `mix pleroma.user toggle_confirmed` to `mix pleroma.user confirm`
- **Breaking**: Changed `mix pleroma.user toggle_activated` to `mix pleroma.user activate/deactivate`
- Polls now always return a `voters_count`, even if they are single-choice.
- Admin Emails: The ap id is used as the user link in emails now.
- Improved registration workflow for email confirmation and account approval modes.
- Search: When using Postgres 11+, Pleroma will use the `websearch_to_tsvector` function to parse search queries.
- Emoji: Support the full Unicode 13.1 set of Emoji for reactions, plus regional indicators.
- Deprecated `Pleroma.Uploaders.S3, :public_endpoint`. Now `Pleroma.Upload, :base_url` is the standard configuration key for all uploaders.
- Improved Apache webserver support: updated sample configuration, MediaProxy cache invalidation verified with the included sample script
- Improve OAuth 2.0 provider support. A missing `fqn` field was added to the response, but does not expose the user's email address.
- Provide redirect of external posts from `/notice/:id` to their original URL
- Admins no longer receive notifications for reports if they are the actor making the report.
- Improved Mailer configuration setting descriptions for AdminFE.
- Updated default avatar to look nicer.
- <details>
  <summary>API Changes</summary>

  - **Breaking:** AdminAPI changed User field `confirmation_pending` to `is_confirmed`
  - **Breaking:** AdminAPI changed User field `approval_pending` to `is_approved`
  - **Breaking**: AdminAPI changed User field `deactivated` to `is_active`
  - **Breaking:** AdminAPI `GET /api/pleroma/admin/users/:nickname_or_id/statuses` changed response format and added the number of total users posts.
  - **Breaking:** AdminAPI `GET /api/pleroma/admin/instances/:instance/statuses` changed response format and added the number of total users posts.
  - Admin API: Reports now ordered by newest
  - Pleroma API: `GET /api/v1/pleroma/chats` is deprecated in favor of `GET /api/v2/pleroma/chats`.
  - Pleroma API: Reroute `/api/pleroma/*` to `/api/v1/pleroma/*`

</details>

### Added

- Reports now generate notifications for admins and mods.
- Support for local-only statuses.
- Support pagination of blocks and mutes.
- Account backup.
- Configuration: Add `:instance, autofollowing_nicknames` setting to provide a way to make accounts automatically follow new users that register on the local Pleroma instance.
- Ability to view remote timelines, with ex. `/api/v1/timelines/public?instance=lain.com` and streams `public:remote` and `public:remote:media`.
- The site title is now injected as a `title` tag like preloads or metadata.
- Password reset tokens now are not accepted after a certain age.
- Mix tasks to help with displaying and removing ConfigDB entries. See `mix pleroma.config`.
- OAuth form improvements: users are remembered by their cookie, the CSS is overridable by the admin, and the style has been improved.
- OAuth improvements and fixes: more secure session-based authentication (by token that could be revoked anytime), ability to revoke belonging OAuth token from any client etc.
- Ability to set ActivityPub aliases for follower migration.
- Configurable background job limits for RichMedia (link previews) and MediaProxyWarmingPolicy
- Ability to define custom HTTP headers per each frontend
- MRF (`NoEmptyPolicy`): New MRF Policy which will deny empty statuses or statuses of only mentions from being created by local users
- New users will receive a simple email confirming their registration if no other emails will be dispatched. (e.g., Welcome, Confirmation, or Approval Required)
- <details>
  <summary>API Changes</summary>

  - Admin API: (`GET /api/pleroma/admin/users`) filter users by `unconfirmed` status and `actor_type`.
  - Pleroma API: `GET /api/v2/pleroma/chats` added. It is exactly like `GET /api/v1/pleroma/chats` except supports pagination.
  - Pleroma API: Add `idempotency_key` to the chat message entity that can be used for optimistic message sending.
  - Pleroma API: (`GET /api/v1/pleroma/federation_status`) Add a way to get a list of unreachable instances.
  - Mastodon API: User and conversation mutes can now auto-expire if `expires_in` parameter was given while adding the mute.
  - Admin API: An endpoint to manage frontends.
  - Streaming API: Add follow relationships updates.
  - WebPush: Introduce `pleroma:chat_mention` and `pleroma:emoji_reaction` notification types.
  - Mastodon API: Add monthly active users to `/api/v1/instance` (`pleroma.stats.mau`).
  - Mastodon API: Home, public, hashtag & list timelines accept `only_media`, `remote` & `local` parameters for filtration.
  - Mastodon API: `/api/v1/accounts/:id` & `/api/v1/mutes` endpoints accept `with_relationships` parameter and return filled `pleroma.relationship` field.
  - Mastodon API: Endpoint to remove a conversation (`DELETE /api/v1/conversations/:id`).
  - Mastodon API: `expires_in` in the scheduled post `params` field on `/api/v1/statuses` and `/api/v1/scheduled_statuses/:id` endpoints.

</details>

### Fixed

- Users with `is_discoverable` field set to false (default value) will appear in in-service search results but be hidden from external services (search bots etc.).
- Streaming API: Posts and notifications are not dropped, when CLI task is executing.
- Creating incorrect IPv4 address-style HTTP links when encountering certain numbers.
- Reblog API Endpoint: Do not set visibility parameter to public by default and let CommonAPI to infer it from status, so a user can reblog their private status without explicitly setting reblog visibility to private.
- Tag URLs in statuses are now absolute
- Removed duplicate jobs to purge expired activities
- File extensions of some attachments were incorrectly changed. This feature has been disabled for now.
- Mix task pleroma.instance creates missing parent directories if the configuration or SQL output paths are changed.
- <details>
  <summary>API Changes</summary>
  - Mastodon API: Current user is now included in conversation if it's the only participant.
  - Mastodon API: Fixed last_status.account being not filled with account data.
  - Mastodon API: Fix not being able to add or remove multiple users at once in lists.
  - Mastodon API: Fixed own_votes being not returned with poll data.
  - Mastodon API: Fixed creation of scheduled posts with polls.
  - Mastodon API: Support for expires_in/expires_at in the Filters.
</details>



