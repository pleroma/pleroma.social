title: MastoFE deprecation, removal in 2021-09
date: 2021-08-09T11:32:00Z
author: lanodan

With the Mastodon Frontend distribution ("MastoFE") having lost collaboration when the glitch-soc maintainer went from "friendly/welcoming to pleroma" to a "fuck pleroma" almost a year ago, it became out of support.  
Currently, MastoFE's last release was made on 2020-05-14, the last commit activity was in 2020-09 and at around 2021-04 it became obvious that MasotFE wasn't going to get any better and would sadly become technical debt.

<!-- more -->

MastoFE was a nice hack, specially when the Mastodon API got introduced, but it kept being clunky, always ended up relying on backends hacks for login and PleromaFE for almost all the settings, making it far from a first-party frontend, as in a frontend that you could entirely switch over seemlessly and not loose essential features.

It was also starting to noticeably lack pleroma-specific features:
<ul>
<li>Chats</li>
<li>Emoji Reactions</li>
<li>Post previews</li>
<li>Post expiration</li>
<li>Moderation integration</li>
<li>Better attachments support</li>
</ul>

And on the maintainance side it lacked what could begin to be called a testsuite (3.71% of coverage just isn't), making it hard to do modifications and releases in a timely manner as real life testing had to be done, and of course bugs were missed too often.

So this is goodbye, MastoFE and it's hacks in the backend is going to be removed from the `develop` branch in 2021-09, so a month after the 2.4.0 release and so will be absent in 2.5.0, the MastoFE repository that I maintained already got archived.

In terms of alternatives I would recommend giving PleromaFE a honest try, but there is <a href="https://docs.pleroma.social/backend/clients/">known alternative frontends</a>, like <a href="https://nicolasconstant.github.io/sengi/">Sengi</a> which has a similar layout and additionnaly offers the ability to use multiple accounts.
