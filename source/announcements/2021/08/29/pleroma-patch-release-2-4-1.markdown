title: Pleroma patch release: 2.4.1
date: 2021-08-29T15:12:00Z
tags: ["stable", "Release"]
author: lanodan

Pleroma 2.4.1 has been released, featuring many fixes.

<!-- more -->

## Upgrade notes

1. Restart Pleroma

## Backend changes

### Changed
- Make `mix pleroma.database set_text_search_config` run concurrently and indefinitely

### Added
- AdminAPI: Missing configuration description for StealEmojiPolicy

### Fixed
- MastodonAPI: Stream out Create activities
- MRF ObjectAgePolicy: Fix pattern matching on "published"
- TwitterAPI: Make `change_password` and `change_email` require params on body instead of query
- Subscription(Bell) Notifications: Don't create from Pipeline Ingested replies
- AdminAPI: Fix rendering reports containing a `nil` object
- Mastodon API: Activity Search fallbacks on status fetching after a DB Timeout/Error
- Mastodon API: Fix crash in Streamer related to reblogging
- AdminAPI: List available frontends when `static/frontends` folder is missing
- Make activity search properly use language-aware GIN indexes
- AdminAPI: Fix suggestions for MRF Policies
