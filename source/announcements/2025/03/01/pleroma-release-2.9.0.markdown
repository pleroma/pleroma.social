title: Pleroma Release: 2.9.0
date: 2025-03-01 00:00 UTC
tags: ["stable", "Release"]
author: lain

Pleroma 2.9.0 is a feature, bugfix and security release. Please read through the changelog to see the changes in detail. As it is a security release, immediate update is recommended.

<!-- more -->

## Upgrade notes

### From source only
1. Get new dependencies and recompile Pleroma:

    ```
    MIX_ENV=prod mix deps.get
    MIX_ENV=prod mix compile
    ```
2. Restart Pleroma

### Everyone

1. Run database migrations (inside Pleroma directory):
  * OTP: `./bin/pleroma_ctl migrate`
  * From Source: `mix ecto.migrate`

2. Restart Pleroma

## Backend changes

### Security
- Require HTTP signatures (if enabled) for routes used by both C2S and S2S AP API
- Fix several spoofing vectors

### Changed
- Performance: Use 301 (permanent) redirect instead of 302 (temporary) when redirecting small images in media proxy. This allows browsers to cache the redirect response. 

### Added
- Include "published" in actor view
- Link to exported outbox/followers/following collections in backup actor.json
- Hashtag following
- Allow to specify post language

### Fixed
- Verify a local Update sent through AP C2S so users can only update their own objects
- Fix Mastodon incoming edits with inlined "likes"
- Allow incoming "Listen" activities
- Fix missing check for domain presence in rich media ignore_host configuration
- Fix Rich Media parsing of TwitterCards/OpenGraph to adhere to the spec and always choose the first image if multiple are provided.
- Fix OpenGraph/TwitterCard meta tag ordering for posts with multiple attachments
- Fix blurhash generation crashes

### Removed
- Retire MRFs DNSRBL, FODirectReply, and QuietReply
