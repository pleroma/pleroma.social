title: Pleroma Release: 2.9.1
date: 2025-03-11 00:00 UTC
tags: ["stable", "Release"]
author: lain

Pleroma 2.9.1 is a security release. It fixes a whole bunch of spoofing issues that were reported to us by the Akkoma developers. Updating is highly recommended.

<!-- more -->

## Upgrade notes

### From source only
1. Get new dependencies and recompile Pleroma:

    ```
    MIX_ENV=prod mix deps.get
    MIX_ENV=prod mix compile
    ```
### Everyone

1. Restart Pleroma

## Backend changes

### Security
- Fix authorization checks for C2S Update activities to prevent unauthorized modifications of other users' content.
- Fix content-type spoofing vulnerability that could allow users to upload ActivityPub objects as attachments
- Reject cross-domain redirects when fetching ActivityPub objects to prevent bypassing domain-based security controls.
- Limit emoji shortcodes to alphanumeric, dash, or underscore characters to prevent potential abuse.
- Block attempts to fetch activities from the local instance to prevent spoofing.
- Sanitize Content-Type headers in media proxy to prevent serving malicious ActivityPub content through proxied media.
- Validate Content-Type headers when fetching remote ActivityPub objects to prevent spoofing attacks.

### Changed
- Include `pl-fe` in available frontends

### Fixed
- Remove trailing ` from end of line 75 which caused issues copy-pasting
