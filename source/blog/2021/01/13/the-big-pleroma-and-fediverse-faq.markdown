title: The Big Pleroma and Fediverse FAQ Part 1 - Beginner Questions
date: 2021-01-13 19:35 UTC
tags: ["faq"]
author: lain

Recent events surrounding social media have made many people think about leaving the currently dominant platforms. If you are looking for alternatives, you'll inevitably come across the term 'Fediverse'. But what is the Fediverse and why should you use it?

In this first in a series of FAQs, I'll try to answer questions that a total beginner might have. In later installments, I'll answer more questions about the technical details and about Pleroma specifics.

I hope this will help you along on your way to the Fediverse!

<!--more-->

### What is the Fediverse?

The Fediverse is a network of websites that talk with each other using a common language - [ActivityPub](https://activitypub.rocks). This means users of those websites can interact with each other no matter what service they use. Imagine commenting on a YouTube video with your Facebook account, or having your Twitter friends comment on your Instagram posts! The Fediverse has alternatives for all of these old social media platforms. There is Mastodon and Pleroma for Twitter-style microblogs, PeerTube to host your videos, PixelFed to show off your photos and many, many more.

To put it simply, it's as if there were a thousand Twitters, a thousand Facebooks, a thousand YouTubes, but they could all talk to each other and you would not need an account on each one.

### What are Instances?

On the Fediverse, domains that run ActivityPub servers are called "instances". For example, lain.com and mastodon.social are instances. You can also say 'site', 'server' or 'node'. All instances together make up the whole Fediverse.

These instances are where users sign up and interact with the fediverse. In general, you only need an account on a single instance, but there's nothing that stops you from making many.

### What is Pleroma?

Pleroma is the best[^1] ActivityPub software there is for general Twitter-style social media. Low on resource requirements and with several web frontends and mobile apps, it'll work well for any kind of community you want to create without breaking the bank. It's also perfect for self-hosters who want to run a one-person setup.

For users, it has a few additional features that many other ActivityPub servers lack, like chats between users and emoji reactions.

### So it's like Twitter?

Yeah it's a lot like Twitter, but much more flexible.

### On Twitter I am @coolkitty420. What would my fediverse handle look like?

On Twitter, handles are just the username. On the Fediverse, the domain name is added to your name so it looks more like an email address. If you would make your account at coolkitty.zone, your full fediverse handle would be @coolkitty420@coolkitty.zone.

If Twitter ever implemented ActivityPub (who knows), your Twitter handle would be @coolkitty420@twitter.com.

### Is the Fediverse only for Twitter-like social networking?

Not at all! There's also a YouTube-like video hosting software called [PeerTube](https://joinpeertube.org/), [Pixelfed](https://pixelfed.org/) for the Instagram crowd and many many more. And because they all speak the ActivityPub standard, you don't need to make an account on a PeerTube server to comment on a video - you can just use your existing Pleroma account.

### I already have a Twitter/Facebook/MySpace account. What's better about the Fediverse?

Many of the advantages of the Fediverse over old social media lie in its decentralized nature. It enables the fediverse to 'square the circle' of moderation and free expression on a massive network because moderation decisions happen locally on a server and not globally on the network. This means that:

1. Your server can freely decide which other servers it wants to block. From the viewpoint of your server, that other instance is gone.
2. If your server is blocked from other people's servers, you can still post on your own server and federate with servers who haven't blocked you. So a moderation decision on another instance does not affect you very much.

This way, each server can have moderation policies its users want while still communicating with the rest of the fediverse. 

Another advantage of this is that servers usually form a true community of like-minded users. With servers being cheap to run, this also practically eliminates the need to run ads to finance a server, as there are usually other monetization strategies.

### What if a hosting provider just 'pulls the plug' to kill the Fediverse?

If a server hosts something that is illegal or against provider's terms of service, it may be shut down. But that barely affects the network as a whole since it would only be one server out of thousands. A network-wide shutdown is not possible.

This is a much more effective way to punish bad actors without significant collateral damage that comes with shutting down centralized social networks. Most of those are shut down because of a few perpetrators but inconvenience many unrelated users.

### So no ads... But how does the Fediverse make money, then?

'The Fediverse' itself doesn't need to make money because it is simply a network of independent servers. Those who run the servers need to cover the related costs. Here are a few ways they can do that:

1. Collect user donations.
2. Charge for sign-ups.
3. Cross-finance from a service that does make money. For example, a chess club could finance their server out of collected membership fees.
4. Run out of one's own pocket as a hobby. Fediverse servers are cheap enough for that.
5. Indeed, a server could even run ads to finance itself. To my knowledge, there's no server that does that at the moment.

### If I want to run my own server, how expensive is that?

This varies depending on how many users you want to support, but as a point of reference, a Pleroma server for a small group (under 50 people) runs fine on a system with 2 gigabytes of RAM and one CPU core. A server like this can be rented for €3/$4 a month. You could also run a server on Raspberry Pi 4, which costs around €50/$55 (model with 4 gigabytes of RAM).

Here's our [installation guide](https://docs.pleroma.social/backend/installation/otp_en/) waiting for you to try it out!

### If I don't want to run my own server, how can I join?

If you already have a friend who is using the fediverse, it is probably best to join the same server and learn how everything works. If you are truly lost, here are a few recommended Pleroma instances you can check out:

1. [Outerheaven.club](https://outerheaven.club/about)
2. [Stereophonic.space](https://stereophonic.space/about)
3. [Shitposter.club](https://shitposter.club/about)

We are also working on a beginner instance that you can use to try out Pleroma and see if it is for you. It is not quite ready yet, so stay tuned.

[^1]: Source: Lain, founder of Pleroma
