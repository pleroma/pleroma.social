title: Releasing Pleroma 2.0.0
date: 2020-03-08 10:52 UTC
tags: ["Release"]
author: lain

<a class="no-style" title="Pleroma-tan drawn by Nightingalle" href="https://shpposter.club/notice/9sjl0p9NbGJsVRuyKe">
![](/images/pleroma_tan_2.0_nac.jpg)
</a>

Long-awaited and finally ready, just in time for ~~FOSDEM~~ International Women's Day, Pleroma 2.0.0 is here! There have been a lot of changes, including breaking ones, so let me give you a quick tour of this release. 

<!--more-->

## Performance

If you read [my blog](https://blog.soykaf.com) in the last few weeks, you've noticed that I have been analyzing Pleroma database performance. I'm happy to report that I found a lot of places to optimize, and a few settings have made their way into this release. There's nothing you have to besides updating Pleroma to activate those. This should lead to more consistent query times and less timeouts. If you want to read all the details, check out [this article](https://blog.soykaf.com/post/postgresql-elixir-troubles/).

The Pleroma frontend has ALSO received a huge performance boost by rewriting one of the libraries we used. If you encountered slow and stuttering behavior in the past, please try it out again. Here as well we will continue to optimize performance in upcoming releases.

## Emoji Reactions

![](/images/emoji_reactions.png)

Nobody asked, and we listened. You can now react with any (well, mostly any) Unicode emoji to any post. These reactions are not the same as likes, so feel free to use them in any situation where they are fun! There is some compatibility with [Misskey](https://github.com/syuilo/misskey) and [Honk](https://humungus.tedunangst.com/r/honk) (although reactions are called 'badonks' there), and people are working on mobile app support as well.

Don't worry, we want custom emoji reactions as well, but they will have to wait for another release :)

## Dropping OStatus support

Pleroma started as an alternative frontend for [GNU Social](https://gnusocial.network/), and it used its OStatus protocol for communication between instances, just like Mastodon. Internally, Pleroma has been always been built on ActivityPub and the concepts don't always map cleanly between those two. With time, OStatus became used less and less on the network, which led to a few security issues in our OStatus code slipping through because that code path was barely used and looked at.

In 2.0.0, we are removing support for OStatus federation and only federate over ActivityPub now. This was not an easy decision and we made it for the following reasons.

1. OStatus is barely used on the network anymore.
2. GNU Social can by now use ActivityPub, see [this repo](https://notabug.org/diogo/gnu-social)
3. OStatus was a constant source of bugs and security issues for us
4. Removing OStatus greatly simplifies the federation code

We always tried to not leave GNU Social instances behind, so we waited until GNU Social supported ActivityPub until we made this decision. Still, if you have friends on non-ActivityPub GNU Social instances, you won't be able to reach them until their servers get updated.

## Admin Improvements

![](/images/admin_fe.png)

Many Pleroma users ask the question: Why do I have to edit configuration files to manage my instance? Well, that won't be an issue anymore from 2.0.0 on.

Our Admin frontend received a lot of love for this release. We now support storing your configuration in the database, so that you should be able to manage your instance without having to edit configuration files. This is the first release in which we support this method of running your instance, so there might still be some rough edges. Please read the [documentation](https://docs-develop.pleroma.social/backend/administration/CLI_tasks/config/) before using it!


## Many more small fixes and additions

From a static frontend over themes 2.0 to rate limiter changes, there's lots of small stuff that changed in 2.0.0. If you do the update, please check the [changelog](/announcements/2020/03/08/pleroma-major-release-2-0-0/), especiall the parts that say **breaking**! You might have to make some small adjustments to your existing configuration files.

## Towards 3.0
<a class="no-style" title="Pleroma-tan drawn by hakui" href="https://tuusin.misono-ya.info/notice/9sjUuIxY7ynWVywfBY">
![](/images/pleroma_tan_2.0_hakui.png =350x350)
</a>

If there's one thing about 2.0.0 I'm not happy about, it's that it took too long to release, tempting server admins to switch to the unstable branch (don't do it!). Our plan for the future is to do actual feature releases more often. This will require some more planning on our side, but it should be worth it. We already have a lot of interesting features in the pipeline, like a new, faster federation transport and switchable frontends.

I want to thank everybody involved with Pleroma for making this release happen! That includes the developers, of course, but also all the users who report issues and test our development code, who help others with configuration and installation and generally spread the word about Pleroma and the Fediverse. Thank you all, and I hope you'll love Pleroma 2.0.0 :)
