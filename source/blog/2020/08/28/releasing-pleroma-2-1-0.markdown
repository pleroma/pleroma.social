title: Releasing Pleroma 2.1.0
date: 2020-08-28 1:00 UTC
tags: ["Release"]
author: lain

<a class="no-style" title="Pleroma-tan drawn by hakui" href="https://pleroma.soykaf.com/notice/9yWL0EmBpZcNKqCEuO">
![](/images/pleroma_tan_2.1_cofe.png)
</a>

Half a year after Pleroma 2.0, today we are releasing our next big version, Pleroma 2.1. Doesn't sound like that much more, but it's full of new features and fixes! Keep reading on to find out what we baked for you.

<!--more-->

## Chats!

![](/images/pleroma_2.1_chat_screenshot.png)

The most prominent feature in 2.1 are the all-new Chats. You can instantly message your friends (and enemies) in a one-to-one chat directly inside Pleroma now - no more "DM for discord"!

This feature is completely different from the existing direct messaging system, which proved to be cumbersome and easy to get wrong. If you want to get the whole story, you can check out this <a href='https://git.pleroma.social/pleroma/pleroma/-/merge_requests/2429'>Merge Request</a>, but the main point is that these Chats will feel and behave like the chats you are used to from other instant messaging systems like XMPP, Matrix or WhatsApp.

For now, we do not have group chats (which will require group support) or end-to-end encryption (check out my <a href='https://blog.soykaf.com/post/encryption/'>blog post</a> about that topic for more info), but these are on the horizon.

Try it out with anyone else who is running Pleroma 2.1 or a recent version of <a href="https://humungus.tedunangst.com/r/honk">Honk</a>. Or just message me at <a href="https://lain.com/users/lain">@lain@lain.com</a>!

## More backend stuff!

Pleroma 2.1 brings more configuration options for admins, like fine-grained restrictions of timelines for registered users and guests, a 'by approval' registrations mode and an easy way to install and manage different frontends.

If you are an emoji fan, you might want to check out our new `EmojiStealPolicy` which automatically populates your local emoji collection by pulling them from friendly instances.

We also now added a by-default MRF policy that will prevent very old posts from showing up in your timeline when they freshly arrive. While this was not a bug, it did confuse many users who didn't know the reason why it happened.

Under the hood, we rewrote most of our code relating to activity ingestion. We also rewrote our HTML parsing library once again, to make it even better. Our planned new HTTP client, however, did not get ready for this release. We hope we can bring it to you in the next one!

As always, if you want to see the full picture, check out the <a href='/announcements/2020/08/27/pleroma-release-2-1-0/'>changelog</a>.

## Frontend changes

![](/images/pleroma_2.1_timelines.png)

As you can see, the frontend changed a bit! Our navigation panel on the left side became quite crowded, so we slimmed it down a bit. There's now only one 'Timeline' link. You can select the different kinds of timelines in a dropdown menu on the timeline itself now.

As always, there are many many more changes and fixes. Check out the <a href='/announcements/2020/08/27/pleroma-release-2-1-0'>PleromaFE changelog</a> if you want to learn more.


## What's coming in 2.1++

We're quite happy with this release, but there are some things that are close to done but didn't quite make it this time:

- Virtual scrolling for better frontend performance
- A websocket-based federation transport for faster and more efficient federation
- User-selectable frontends
- A thumbnailing media proxy that will save you lots of data
- Rich tooltips for user profiles
- Theming and settings improvements

We'll be working hard on these and will try to release them soon!

And one more thing, of course...

![](/images/groups.jpg)

Enjoy Pleroma 2.1!
