title: Gopher Support in Pleroma
date: 2018-04-01T04:01:00+02:00
tags: ["Gopher", "April Fools", "rokalife"]
author: lain
images: [
  "/images/no-js.png"
] # overrides the site-wide open graph image

<small> *Note: this has originally been posted at <https://blog.soykaf.com/post/gopher-support-in-pleroma/>* </small>

As you may already know, [Pleroma](/) is high-performance and low-resource fediverse server mean to run even on small devices like a Raspberry Pi.

Still, there is one part of Pleroma that is wasteful to the extreme...

![](/images/no-js.png)

It's true! Pleroma's mother allowed it to have TWO frontends, but both are resource-hogging Javascript monstrosities. If you don't have a 'modern' browser that turns your PC into a heater, you won't be able to see any posts!

So where did we go wrong? I think the problem goes way back. We made a deal with the Devil when we accepted free-form HTML into our systems. The best solution is to not make this mistake anymore, and go back to the future to a better alternative.

And that's why, today, I'm announcing [Gopher](https://en.wikipedia.org/wiki/Gopher_(protocol)) support for Pleroma!

<!--more-->

You can try it out right now! Install a gopher compatible browser like Lynx and run `lynx gopher://pleroma.soykaf.com:9999`. This is what you'll see.

![](/images/lynx-1.png)

Simplicity is perfection! You can easily read any of the public timelines through this.

![](/images/lynx-2.png)

And of course you can also see conversations in a thread view. This should be enough for 90% of our social network needs, while being a hundred times more efficient than HTML, with JS or without.

![](/images/lynx-3.png)

I hope that with this recent change, it is clear once again that Pleroma is always innovating for better resource usage and efficient computing. Have fun and see you on the fediverse!
