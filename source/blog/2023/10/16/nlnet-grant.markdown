title: We got a grant from NLNet!
date: 2023-10-16 15:27 UTC+2
tags: ["finances"]
author: hj

Hi, HJ here. After spring's disaster and previous years of mismanagement and never-ending drama some folks in pleroma-dev have been pretty demotivated and burned out. Having pleroma as a side "weekend warrior" kind of deal doesn't help it either, as most of us have jobs/schools and other stuff to do and people would rather spend weekend relaxing. not to mention, I personally would really love working on Pleroma fulltime, but alas it doesn't pay bills. After seeing a whole bunch of fediverse projects on NLnet I thought "why the heck not" and with the rest of dev community's blessing, send the application. Several months and bunch of paperwork later we got approved.

And here it is: https://nlnet.nl/project/Pleroma/

Since we have somewhat a history of being not-entirely-transparent about being financed by someone let's rectify this right now, in a bit of a QnA format.

**Q:** Is this another (hostile) takeover?  
**A:** No. At least I hope it doesn't come as such. I just want people to be able to spend more time working on project. I don't intend on telling people what to do and project direction is really up to community's decision.  

**Q:** How much?  
**A:** Around 30k€ for a year.  

<!--more-->
**Q:** Did someone get a briefcase full of cash and now distributes it along the devs?  
**A:** No, we basically got a signed *promise* that we'll receive the money, if we complete the *tasks*, whomever done it can ask for pay for that task.  

**Q:** So it is like a bounty program?  
**A:** Not really, since only people "on the list" can do that. The list is flexible and we can add people there but it also means we need to know your official name and address. This is really just for the suits, so that EU who gives money to NLNet knows they aren't just taking money for themselves through relatives etc.  

**Q:** What are the "tasks" you talk about? Does this mean EU or NLNet tells you what to do?  
**A:** No, the tasks were defined by ourselves (mostly me with help of other members), split into smaller milestones. According to an NLNet representative it's philanthropy, they can't and won't make demands we do specific tasks they want, they can still suggest things and try out software themselves though, much like anybody. And we not obligated to finish those tasks within time frame either - it just means we don't get the money. Financing could be extended though.  

**Q:** What are the tasks you defined exactly?  
**A:** In no particular order those are:  

 * Admin dashboard in PleromaFE
 * Improved mobile experience (PWA, performance improvements, UI scale, settings sync etc)
 * Chats improvements (ability to display (DM) threads as chats, shoutbox improvements)
 * Instance customization and QoL
 * Personal Data and Experience management (managing your uploads, timed mutes, regex blocks, username changes, expiring posts etc)  
 
They are split into various smaller tasks, listing all of them here would be too much, might need another post or a page.  

**Q:** Who's on "the list"?  
**A:** Currently it's me (HJ), tusooa, lanodan. I act as a manager for this whole thing, which means I also have to approve the payment requests, I have to approve adding people to the list etc.  

**Q:** Is the list of tasks and people set in stone?  
**A:** No, everything is amendable. No I don't know exactly how amendable it is?  

**Q:** Does this mean those issues are ALL you work on?  
**A:** No, we still have our duty to fix the bugs and everyone free to work whatever they want. I really wish (and it was my initial pitch/grant size estimate) to just give people small "salary" per month to cover the rent and let them do whatever but that's just not how it works.  

**Q:** So you're being financed now, I can stop donating?  
**A:** That is up to you. Donations and support is always welcome and never mandatory. Your donations did help me personally once at least, so there's that.  

This is just a short PSA/overview for sake of transparency, more details are/will be in the inter-team communications (IRC/gitlab). If you have any questions - contact me at https://post.ebin.club/users/hj or https://shigusegubu.club/users/hj

T. HJ
