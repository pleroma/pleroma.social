title: Fighting the Bloat! SSH Support in Pleroma
date: 2019-04-01T04:01:00+02:00
tags: ["BBS", "SSH", "April Fools", "rokalife"]
author: lain
images: [
  "/images/rokalife.png"
] # overrides the site-wide open graph image
<small> *Note: this has originally been posted at <https://blog.soykaf.com/post/ssh-support-in-pleroma/>* </small>

Pretty much one year ago, we started fighting the typical Web 4.0 Javascript bloat by adding [Gopher support](/blog/2018/04/01/gopher/). After a wildly successful [0.999 release](/blog/2019/02/22/pleroma-0-9-9/), we heard your calls for more debloating. The biggest complaint with the gopher support was the inability to post. So today, we're taking it a step further: Making Pleroma a BBS.

![](/images/pleroma-ssh-help.png)

<!--more-->

If you are a time traveller from the 19th century, you might remember that there was a time before the internet. It was the [age of the BBS](https://archive.org/details/BBS.The.Documentary), a technology now long forgotten.

A BBS (Bulletin Board System) is a program someone used to run on their computer that would make it possible for other people to call in - literally - with a modem and read and send messages, chat and do general social things. They were so popular that some people got huge phone bills because they dialed in so much. Why did they do it? Same reason we use social networks today: They didn't have any friends!

One big advantage of the BBS format in comparison to HTML/JS is that it is text-only, no markup at all, no images and no virtual machine needed to subsidize the RAM industry. Now, we know that you probably don't have an actual modem at home, and most servers won't be attached to one. So here's the next best thing to a number you can dial in to: An ssh server!

<video controls class=fit>
  <source src='/images/bbs-recode.mp4' type='video/mp4' />
</video>

As you can see, you can log in with your existing account an password and enjoy the super high speed of modern pipes with text-only interfaces. You can check your home timeline, post something, and even reply! Is it useful? Not really, but I'm sure rinpatch will fix that!

So how do you actually run this on your server? Well, it is still in development, but you can try it out right now by checking out the [BBS](https://git.pleroma.social/pleroma/pleroma/tree/feature/bbs) branch over at our Gitlab server. Just follow the instructions in `docs/config.md` and you should be running your own BBS in no time! Hack the planet!
