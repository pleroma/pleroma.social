title: Pleroma's First Release! 0.9.9
date: 2019-02-22T13:58:34+01:00
tags: ["Release"]
author: lain
images: [
  "/images/0.9.9.screenshot.png"
] # overrides the site-wide open graph image
---
<small> *Note: this has originally been posted at <https://blog.soykaf.com/post/pleroma-release-0.9.9/>* </small>

The [first commit](https://git.pleroma.social/pleroma/pleroma-fe/tree/191c02af1ebfc7e6c53dc88d97c4e3ca23fbea8b) in Pleroma happened 2016, on October 26th. Now it's a few years and months later, and we are doing our first stable release!

Are you still running `develop`? That's so 2018.


![](/images/0.9.9.screenshot.png)

<!--more-->

[Pleroma](https://pleroma.social) has been growing a lot last year. According to [fediverse statistics](https://fediverse.network), we now have over 400 instances online on the network. We are seeing more people use it everyday, because it is easy to setup and runs on small devices like a Raspberry Pi.

For a rather long time, only I was running a Pleroma instance. For another long time, maybe 10 or 20 other instances were online. With a community of that size, it was easy to tell everyone to just use the development branch and notify them when something breaky happened. But by now we have around 80 contributors and over 400 admins and not every piece of code is written or reviewed by me. This led to a few rather rough upgrades, which could have been avoided by having a stable branch.

![](/images/0.9.9.screenshot-masto.png)

So today, I am releasing the first stable Pleroma release, Pleroma 0.9.9 (a very strong release, maybe the strongest!). So why should you use it? Here are the nice things about it:

- Only tested features in it
- Will receive bugfixes
- Will have changelogs between releases
- I run it on my server so I'll know what breaks :)

By now, Pleroma has become a useful tool for thousands of users and this stable release is a great milestone to promote this.

If you want, you can still run on the `develop` branch, but expect this one to be in flux a lot.

Thanks to everyone who works on Pleroma! Thanks to everyone who reports bugs! And a big thank you if you are using Pleroma! Let's continue to make Pleroma better together in 2019!

You can find the release at [our Gitlab](https://git.pleroma.social/pleroma/pleroma/tags/v0.9.9) or download the tar.gz right [here](/misc/pleroma-v0.9.9.tar.gz).
