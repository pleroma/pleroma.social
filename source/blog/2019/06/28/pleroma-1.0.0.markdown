title: Pleroma 1.0.0
date: 2019-06-28T14:37:36+02:00
author: lain
tags: ["Release"]
images: [
  "/images/pleroma_1.png"
]

<small> *Note: this has originally been posted at <https://blog.soykaf.com/post/pleroma-1.0/>* </small>

After long months of doing only bugfix and security releases, we are finally releasing the long-awaited 1.0 release. No more nines!

![](/images/pleroma_1.png)

<!--more-->
[Pleroma 1.0.0 is here!](https://git.pleroma.social/pleroma/pleroma/-/tags/v1.0.0)

By now, you all know how great [Pleroma](https://pleroma.social) is. It is your favorite free software federated social network, written in Elixir. If you don't really know it yet, check out [this post]({{< relref "what-is-pleroma.md">}}).

We think we have achieved a state of the project that we can call 1.0. Let me talk a bit about all the new stuff we have in store for you.
## Polls

![](/images/pleroma_polls.png)

You know them from your second-favorite social network, and now you can also make funny polls in Pleroma. The rules for them are a bit more relaxed than elsewhere: You can make a lot of options and attach pictures if you want. Want to know if Rei or Asuka is best girl? You can find out!

## Docs!

[DOCS!](https://docs.pleroma.social) Finally you don't have to find us on IRC anymore to figure out how to install new emojis.

## OTP Releases!

OTP releases should be very interesting for you if you are hosting your own Pleroma server. They bundle the compiled Pleroma code with the necessary runtime files of Erlang and Elixir, so you won't need to install those seperately. It is kind of like 'binary releases' for Pleroma. Check out the [documentation](https://docs-develop.pleroma.social/otp_en.html#content) and how to [switch](https://docs-develop.pleroma.social/migrating_from_source_otp_en.html#what-are-otp-releases) from the current source based installation method. It will also allow you to change configuration settings without recompilation.

A word of warning: This method of release is still slightly experimental, so if you encounter anything weird, please file an issue on the [tracker](https://git.pleroma.social/pleroma/pleroma/) and switch back to the source based installation if needed.

## Lots of other stuff!

Check out the [changelog](https://git.pleroma.social/pleroma/pleroma/-/tags/v1.0.0) and the [docs](https://docs.pleroma.social) to find more cool new stuff, like emoji packs, mix tasks that will help you keep your database small, mongooseim integration to share accounts with an XMPP server... and so on :)

## Thanks!

First I want to thank YOU for reading my blog post. 

Then I want to thank all the people who work on Pleroma. There are too many by now too mention by name, and I'm thankful for all of them. You made all this possible. Without code to base it on, there wouldn't be a fediverse community.

I also want to thank everybody who contributes to Pleroma in a way that's not code. If you write issues in the bug tracker, run an instance for yourself or other, or donate your time and money in another way, it is very appreciated.

Last, I want to thank the fediverse community. Working on Pleroma is fun for the technology, but you make it fun to actually use this stuff :) 

Look forward to a great year for Pleroma and the Fediverse. Let's make 2.0 happen sooner :)

Now I'm off to Pyongyang to escape all this social network stuff, see ya!
