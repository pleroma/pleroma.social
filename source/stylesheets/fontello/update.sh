#!/bin/sh
# SPDX-FileCopyrightText: 2017-2023 Pleroma Authors <https://pleroma.social>
# SPDX-License-Identifier: CC-BY-4.0
WORKDIR="$(dirname "$0")"
SERVER="https://fontello.com"

set -e

command -v curl >/dev/null
command -v bsdtar >/dev/null

set -x

curl --silent --show-error --fail --output .fontello --form "config=@${WORKDIR}/config.json" "${SERVER}"
curl --silent --show-error --fail https://fontello.com/`cat .fontello`/get | bsdtar --strip-components=1 -xvf- -C "${WORKDIR}"
