// SPDX-FileCopyrightText: 2017-2022 Pleroma Authors <https://pleroma.social>
// SPDX-License-Identifier: CC-BY-4.0

function toggleMenu () {
  const content = document.getElementById('menu-content')
  const visibility = content.style.visibility
  if (visibility === '')
    content.style.visibility = 'visible'
  else
    content.style.visibility = ''
}

function init () {
  // Add click listener and remove the hover-based fallback
  const menuButton = document.getElementById('menu-button')
  menuButton.addEventListener('click', toggleMenu)
  menuButton.classList.remove('nojs-menu')
}

init()