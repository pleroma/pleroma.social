#!/bin/sh
# SPDX-FileCopyrightText: 2017-2022 Pleroma Authors <https://pleroma.social>
# SPDX-License-Identifier: CC-BY-4.0

set -e

cat <<EOF
<!DOCTYPE html>
<html xmlns:xi="http://www.w3.org/2001/XInclude" xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <xi:include href="${SRCDIR}/source/layout/base_head.xml" parse="xml" xpointer="xpointer(/xml/*)" />
    <title>${TITLE:-Pleroma Blog}</title>
    <link rel="stylesheet" href="/stylesheets/blog.css"/>
  </head>
  <body>
    <xi:include href="${SRCDIR}/source/layout/navbar.xml" parse="xml"/>
    <article class="article-list">
EOF

for page in $(find source/"$1" -type f -name '*.markdown' | sort -r)
do
	title=$(lowdown -Xtitle "$page")
	date=$(lowdown -Xdate "$page")
	page_link=$(echo "$page" | sed -e 's;^source/;/;' -e 's;\.markdown$;;')

	cat <<EOF
		<div class="article-short">
			<div class="article-meta article-short-date">
				<a href="${page_link}"><time>${date}</time></a>
			</div>
			<div class="article-short-summary">
				<a class="article-short-title" href="${page_link}"><h2>${title}</h2></a>
				<div class="article-short-summary-content">
EOF

			sed -n '0,/<!-- *more *-->/p' "$page" \
			| lowdown -Thtml -

	cat <<EOF
				</div>
			</div>
		</div>
EOF
done

cat <<EOF
    </article>
    <xi:include href="${SRCDIR}/source/layout/footer_${1}.xml" parse="xml"/>
  </body>
</html>
EOF
