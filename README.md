<!--
SPDX-FileCopyrightText: 2017-2022 Pleroma Authors <https://pleroma.social>
SPDX-License-Identifier: CC-BY-4.0
-->

# pleroma.social

This repository contains the source code of <https://pleroma.social>. The website is automatically deployed when the changes are pushed to the `master` branch of this repository.

## Setting up a local development environment

Dependencies:
- POSIX Shell
- libxml's `xmllint`
- [lowdown](https://kristaps.bsd.lv/lowdown/)

**Note**: Be sure to have Git LFS set up for this repository.

Building is simply done via `./build.sh`
