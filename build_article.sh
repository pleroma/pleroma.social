#!/bin/sh
# SPDX-FileCopyrightText: 2017-2022 Pleroma Authors <https://pleroma.social>
# SPDX-License-Identifier: CC-BY-4.0

set -e

title=$(lowdown -Xtitle "$1")
date=$(lowdown -Xdate "$1")
author=$(lowdown -Xauthor "$1")

cat <<EOF
<!DOCTYPE html>
<html xmlns:xi="http://www.w3.org/2001/XInclude" xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <xi:include href="${SRCDIR}/source/layout/base_head.xml" parse="xml" xpointer="xpointer(/xml/*)" />
    <title>${title}</title>
    <link rel="stylesheet" href="/stylesheets/blog.css"/>
  </head>
  <body>
    <xi:include href="${SRCDIR}/source/layout/navbar.xml" parse="xml"/>
    <article>
      <div class="article-meta"><time>${date}</time></div>
      <h1>${title}</h1>
EOF

lowdown -Thtml "$1"

cat <<EOF
      <div class="article-authors">— ${author}</div>
    </article>
    <xi:include href="${SRCDIR}/source/layout/footer_${blog_target}.xml" parse="xml"/>
  </body>
</html>
EOF
