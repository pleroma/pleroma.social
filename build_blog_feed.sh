#!/bin/sh
# SPDX-FileCopyrightText: 2017-2022 Pleroma Authors <https://pleroma.social>
# SPDX-License-Identifier: CC-BY-4.0

set -e

test -n "$1"

last_mod=$(git log -1 --format="%ad" --date=iso-strict -- "source/${1}")

cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
	<title>${TITLE:-Pleroma Blog}</title>
	<id>https://pleroma.social/${1}</id>
	<link href="https://pleroma.social/${1}"/>
	<link href="https://pleroma.social/${1}/feed.xml" rel="self"/>
	<updated>${last_mod}</updated>
EOF

for page in $(find source/"$1" -type f -name '*.markdown' | sort -r)
do
	title=$(lowdown -X title "$page")
	date=$(lowdown -X date "$page")
	date_mod=$(git log -1 --format="%ad" --date=iso-strict -- "$page")
	page_link=$(echo "$page" | sed -e 's;^source/;/;' -e 's;\.markdown$;;')

	cat <<EOF
	<entry>
		<title>${title}</title>
		<published>${date}</published>
		<updated>${date_mod}</updated>
		<link rel="alternate" href="https://pleroma.social${page_link}/"/>
		<id>https://pleroma.social${page_link}/</id>
		<content type="xhtml">
			<div xmlns="http://www.w3.org/1999/xhtml">
EOF

		lowdown -Thtml "$page"

	cat <<EOF
			</div>
		</content>
	</entry>
EOF
done

cat <<EOF
</feed>
EOF
